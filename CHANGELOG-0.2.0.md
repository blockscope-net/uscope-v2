CHANGELOG 0.2.1

Features:

- Config json file to avoid recompilation on config change
- Telegram integration
- New alert messages: start of new oracle window
- New metrics message: displays metrics for all the validators and network status
- Alerts count limit: for peggo offset and oracle miss error, customizable through config
- Independent time intervals: for peggo, validator and ping alerts
- Improved alert messages content and formatting (window change, window progress, )
- Improved ping message: sync, peggo, oracle, and accounts info
- Show stats on startup: alerts, metrics, network status
- Send long messages by splitting them into smaller ones

Docs:

- Documentation update

CHANGELOG 0.2.2

Features:

- Config option for disabling all validators metrics in ping alert
- New alert for non configured validator exchange rates pairs
- Added column for missing exchange rates pairs in validator
- Added message of missing exchange rates pairs in ping alert
- Added network params message with full network params
- Created config template file for reuse (config-template.json)

Fixes:

- Alert on peggo syncing back after offset alert

Docs:

- Updated docs with new features

CHANGELOG 1.0.1

Features:

- Migration to Cobra
- CLI commands to get information about the oracle
- bug fixes
