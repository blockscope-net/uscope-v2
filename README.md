# 🔬 uScope

uScope (MicroScope) is a Sei monitoring tool to alert you from your oracle (price feeder) becoming a zombie.
<br/>

![MicroScope Sei Oracle](software-ad-uscope.png)

## 🗒 Description

uScope is a monitoring and alerting tool for oracle nodes, it aims to be simple to install and use, and useful in features provided.

uScope displays your oracle node's info via several communication channels: Discord, Telegram and the shell.

It also provides commands to get oracle's info on demand.

## 💾 Installation

uScope is written in Go Lang as most Cosmos/IBC tools.

### **Install dependencies**

```sh
sudo apt install curl git -y
```

### **Clone the repository**

```sh
cd ~
git clone https://gitlab.com/blockscope-net/uscope-v2.git
cd uscope-v2
git checkout v1.0.6
```

## ⌨️ Commands Usage

First you need to know is that all the commands have a help you can query with the -h or --help flag.

### Commands help

```sh
# display general help
uscope --help
# display monitor command help
uscope monitor -h
# display get command help
uscope get -h
```

### Get command

Used to get information about the oracle like node status, parameters, exchange rates, etc.

```sh
# get oracle parameters
uscope get -p

# get oracle rates
uscope get -r

# get oracle status
uscope get -s

# get validators oracle stastus
uscope get -v
```

### Monitor command

Used to launch a cron job that monitors your oracle node and send alerts to the communication channels configured (Discord, Telegram).

```sh
# launch monitor process with default alerts
uscope monitor

# launch monitor process and enable network alerts
uscope monitor -n

# launch monitor process and enable ping messages
uscope monitor -p

# launch monitor process and enable sync alerts
uscope monitor -s

# launch monitor process and enable validator list alerts
uscope monitor -v

# launch monitor process and enable all alerts
uscope monitor -npsv
```

## 🏃 Run monitoring

To run uScope you can just execute the binary or create a service unit to get it always running (check next section). Have in mind that the config file should be in the user's home directory.

```sh
uscope monitor -nps
```

## 😈 Run with a Service

As a monitoring tool it's better to handle its execution with a Linux service, so it starts on boot/reboots and restarts in the case it exits the execution.

### **Create service file**

```sh
USCOPE_PATH=$HOME/uscope-v2

echo "[Unit]
Description=uScope Service

[Service]
User="$USER"
Group="$USER"
Environment=PATH=$PATH:$USCOPE_PATH
ExecStart="$USCOPE_PATH/uscope monitor -n -p -s"
WorkingDirectory="$USCOPE_PATH"
Restart=on-failure
RestartSec=5s

[Install]
WantedBy=multi-user.target " > uscope.service
```

### **Move service file to services location**

```sh
sudo mv ./uscope.service /etc/systemd/system/uscope.service
```

### **Enable the service**

```sh
sudo systemctl daemon-reload
sudo systemctl enable uscope
```

### **Run the service**

```sh
sudo systemctl start uscope
```

### **Check logs / output**

```sh
sudo journalctl -u uscope -fo cat
```

### **Stop the service**

```sh
sudo systemctl stop uscope
```

### **Check service status**

```sh
sudo systemctl status uscope
```

## 🏓 Usage

As a monitoring/alerting tool the main thing in uScope is sending you alerts of important events on your Sei oracle (price feeder). Here is a description on how the info messages are structured and the meaning of its parts.

### **Alerts**

A uScope alert message consists of four parts in this order:

- Category: an emoji indicating the category of the message
- Section: a text in capital letters denoting the type of the message
- Content: a meaningful message depicting the event and related info

### **Categories**

Are represented with a coloured ball emoji representing a type of message.

- `🔵 INFO: messages that just communicate interesting info`
- `🟠 WARN: messages that alert from something important that could indicate some issue`
- `🔴 ERROR: messages that alert from a problem that is critical`
- `🟢 OK: messages that communicate that a WARN or an ERROR has been reverted`

### **Sections**

Are represented with an emoji related to the part of the system that the alert regards to.

- `SYNC: alerts to notice from validator syncronization status`
- `ORACLE: alerts to notice from price-feeder oracle important events (oracle misses, start of new window, missing exchange rates, etc.)`

### **Messages**

A uScope info message can vary in structure and info depending on the message. You can expect the following messages

- `BOOT: message displayed at program start up with validator info and external systems integration`
- `PING: periodic messages with summarized info of the system parts (oracle, node, RPCs, etc.)`
- `NETWORK METRICS: periodic messages displayed after ping messages with all validators metrics (moniker, address, oracle misses, etc.)`
- `NETWORK PARAMS: periodic messages displayed after network metrics messages with some network overall stats (oracle stats and parameters)`

## 👨‍🔧 Configuration

### **Config**

In order to config uScope to work with your node, you need to set up values inside the .uscope.json configuration file. To make config changes effective just restart uScope binary/service. Config file must be named .uscope.json and under the $HOME directory for the user executing uScope.

- Comments in this doc will break the json format, so copy `config-tpl.json` to `~/.uscope.json` to create your own.

```json
{
  "cosmos": {
    "rpc": "http://localhost:1317", // your node's API (check your RPC/validator app.toml "API Configuration" section)
    "operatorAddress": "seivaloper....address", // your validator operator address
    "denom": "usei" // token denom (leave as it is)
  },
  "discord": {
    "username": "uScope Sei Mainnet", // user name of the message sender
    "url": "https://discord.com/api/webhooks/....webhook_id" // your Discord webhook url you want to send messages to
  },
  "telegram": {
    "botId": "....telegram_bot_id", // your Telegram bot Id you want to send messages to
    "chatId": "....telegram_chat_id" // your Telegram chat Id you want to send messages to
  },
  "pagerduty": {
    "integrationKey": "....pagerduty_integration_key" // your PagerDuty service integration key
  },
  "slack": {
      "oauthToken": "xoxb-531...73-53....87-0sj....qP", // your slack app bot token (xoxb-)
      "channelId": "...slack_channel_id" // your slack channel id to send messages to
  },  
  // Intervals represented in seconds.
  "monitor": {
    "oracleWindowSize": 201600, // leave it as it is
    "oracleMissesInterval": 60, // time interval for Oracle request misses monitoring
    "validatorInterval": 60, // time interval for validator synchronization monitoring
    "pingInteral": 3600, // time intervar for sending ping and network status messages
    "oracleMissesThreshold": {
      // if oracle misses > 5% send Error alert, if > 1.5% send Warning alert
      "Warn": 1.5,
      "Error": 5
    },
    "oracleNetworkMissesAvgThreshold": {
      // if network oracle average misses > 5% send Warning alert
      "Warn": 5
    },
    "alertMaxAlerts": 5 // Max number of alerts to send
  }
}
```

### **Discord / Telegram / PagerDuty / Slack**

Besides showing logs in the shell terminal uScope communicate with your own Discord account via webhooks and your Telegram app via a bot, for that you'll need to set up your Discord channel and get its webhook, or setup a Telegram bot and get its botId and chatId, or set your PagerDuty service integration key to communicate with. You can find tons of resources for doing that.

Check the config file for the corresponding variable and change the values.

```json
  "discord": {
    "username": "uScope", // user name of the message sender
    "url": "https://discord.com/api/webhooks/....webhook_id" // your Discord webhook url you want to send messages to
  },
```

```json
  "telegram": {
    "botId": "....telegram_bot_id", // your Telegram bot Id you want to send messages to
    "chatId": "....telegram_chat_id" // your Telegram chat Id you want to send messages to
  },
```

```json
    "pagerduty": {
        "integrationKey": "....pagerduty_integration_key"
    },
```

```json
    "slack": {
        "oauthToken": "xoxb-531...73-53....87-0sj....qP", // your slack app bot token (xoxb-)
        "channelId": "...slack_channel_id" // your slack channel id to send messages to
    },  
```

uScope will only send messages to Discord, PagerDuty, Slack and/or Telegram if configured. So if for example, you don't want to send messages to Telegram, just set the corresponding values as empty strings. Like the following.

```json
  "telegram": {
    "botId": "",
    "chatId": ""
  },
```

## 👀 Visuals

### Validators Status

![Validators Status](uscope-shell-ss-1-validator-status.png)

### Exchange Rates

![Exchange Rates](uscope-shell-ss-2-exchange-rates.png)

### Oracle Params

![Oracle Params](uscope-shell-ss-3-oracle-params.png)

### Oracle Status

![Oracle Status](uscope-shell-ss-4-oracle-status.png)

### Discord Alerts

![Discord Ping](uscope-shell-ss-5-discord-init.png)

### Discord Validators Status

![Alt text](uscope-shell-ss-6-discord-validators.png)

## 💡 Troubleshooting

- uScope throw RPC errors at startup causing a panic exit. RPC server must be configured in cosmos.rpc field in config.json file, especifying ip and port (for example: http://localhost:1317) API server must be enabled (.sei/config/app.toml file in [api] section) in RPC server.

```toml
[api]
enable = true
address = "tcp://0.0.0.0:1317"
```

```sh
# check access to the RPC API
curl http://localhost:1317/cosmos/staking/v1beta1/validators
```

- Why doesn't my shell show the Suiss UI colors?
  Probably your shell is not in 256 color mode. You can set that in your .bash_profile (or correspoding shell config file)

In bash:

```
export TERM=xterm-256color
```

In zsh:

```
export TERM=screen-256color
```

If your using Tmux and the problem persist, check this thread to config your Tmux https://unix.stackexchange.com/questions/1045/getting-256-colors-to-work-in-tmux

## 🛣 Roadmap

Release v1.0.0 (10th January 2024)

- Migration from v0.2.0
- Add CLI `get` commands to print info about the oracle
- Add Telegram and PagerDuty integration

## 📬 Support

Contact us throught Gitlab or via email at blockscope@protonmail.com

## ✅ Project status

Alive! We keep updating the project with bug fixes and improvements, don't hesitate to submit an issue to the repository.

## 🦠 Authors and acknowledgment

Developed by Blockscope for the Sei ecosystem.

If you use and like uScope, feel free to:

- stake SEI to our [Blockscope Validator](https://sei.explorers.guru/validator/seivaloper14u38cl6knqxs6vs7lj7vzfvap42yyc3runtrwc): `seivaloper14u38cl6knqxs6vs7lj7vzfvap42yyc3runtrwc`
